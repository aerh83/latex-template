# Template für Abschlussarbeiten in Latex

Dies ist ein Template um Abschlussarbeiten in Latex verfassen zu können. 
Es bietet eine breite Menge an Konfigurationsmöglichkeiten und soll den Einstieg in die Arbeit möglichst vereinfachen.
Die Arbeit selbst bereitet im Schnitt bereits genug Stress.
Prinzipiell sollte sich das Template für Praktikumsberichte, Bachelor- und Masterarbeiten eignen.
Bei Problemen für eine spezifische Anwendung des Templates sind Merge Requests gerne gesehen.

Das Template setzt auf der [Arbeit von Kevin-Horst Bratzke](https://www.thm.de/mnd/fachbereich/organisation/dokumente/abschlussarbeit-latex-vorlage-malerczyk-1/download) auf.
Dieses Template wird in der Empfehlung für das Schreiben von Abschlussarbeiten von Herrn Schultes empfohlen.
Die Funktionalität und das Layout des initialen Templates wurden nicht angepasst, sodass eine Arbeit mit diesem Template ebenfalls kein Problem darstellen sollte.

## Setup

Prinzipiell reicht es das Projekt herunterzuladen, die Konfiguration anzupassen und neue Kapitel einzufügen. 
Eine genauere Beschreibung dieser Schritte findet ihr hier.

1. [Downloade](https://git.thm.de/aerh83/latex-template/-/archive/master/latex-template-master.zip) das Projekt. 
   Alternativ kannst du das Projekt auch forken und ebenfalls mit Git deinen Fortschritt tracken. 
   Für die meisten Nutzer wird aber der simple Download die einfachste Variante sein.
   
2. Importiere das Projekt im Latex-Editor deiner Wahl.

3. Passe die Datei in `config/variables` deinen Anforderungen entsprechend an. 
   Keine Angst: Jede einzelne Konfigurationsoption ist dort dokumentiert.
   Ich würde aber empfehlen diese auch tatsächlich zu lesen. 
   Minimal solltest Du zumindest `isRelease` auf `true` stellen um deine eigenen Kapitel sowie Bibliographie anzuzeigen.
   
4. Beginne dein erstes Kapitel in `chapters/chapter_one.tex` zu schreiben.

5. Um neue Kapitel einzufügen musst Du diese in dem File `chapters/chapter_list.tex` eintragen.
   Halte dich hierbei einfach an die Eintragung von `chapter_one`.
   
Das war es auch schon. Ich hoffe, dass mit diesem Template ein bisschen weniger Arbeit anfällt als ohne. :) 

Viel Erfolg!